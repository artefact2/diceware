diceware
========

My own take on
[Diceware](http://world.std.com/~reinhold/diceware.html) dictionary
generation and formatting for printing. Released under the WTFPLv2.

This package provides three scripts:

* `gen-dict` will generate a word list based on input text, optionally
  rejecting words above a certain length or too infrequent words.

* `format-dict` will format a generated dictionary for printing, and
  index each word using the optimum number of dice rolls, based on
  which dice you have available.

* `gen-passphrase` can generate random passphrases without printing or
  rolling dice, instead using the computer's CSPRNG. Use at your own
  risk.

Read also the excellent [Diceware
FAQ](http://world.std.com/%7Ereinhold/dicewarefaq.html).

Example dictionaries
====================

Dictionaries were generated from Steven Erikson's __Malazan Book of
the Fallen__ series. Only words that appeared twice or more were kept.

* [6 letters and under, requires 2d30 and 1d10, 13.1 bits per word](./demo/mbotf-62-303010-1314.pdf)
* [7 letters and under, requires 4d6 and 1d10, 13.7 bits per word](./demo/mbotf-72-666610-1366.pdf)
* [8 letters and under, requires 2d8 and 4d4, 14.0 bits per word](./demo/mbotf-82-884444-1400.pdf)

Password strength & entropy
===========================

> “To provide adequate protection against the most serious
  threats... keys used to protect data today should be at least 75
  bits long. To protect information adequately for the next 20 years
  ... keys in newly-deployed systems should be at least 90 bits long.”

Quote from __Minimal Key Lengths for Symmetric Ciphers to Provide
Adequate Commercial Security__ (M. Blaze, W. Diffie, R. Rivest,
B. Schneier, T. Shimomura, E. Thompson, and M. Weiner), **published in
1996 (!)**.

~~~
                        | 5 words | 6 words | 7 words | 8 words
------------------------+---------+---------+---------+--------
13 bits/word            |    65.0 |    78.0 |    91.0 |   104.0
13 bits/word + 1 symbol |    75.1 |    88.4 |   101.6 |   114.8
14 bits/word            |    70.0 |    84.0 |    98.0 |   112.0
14 bits/word + 1 symbol |    80.4 |    94.6 |   108.9 |   123.1
~~~

Adding a symbol
===============

Pick a random symbol in the table below by rolling two 8-sided die.
Roll one more dice to figure out in which word the symbol should go,
then one final dice to figure out the position of the symbol in the
chosen word.

~~~
2d8 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
----+---+---+---+---+---+---+---+--
  1 | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7
  2 | 8 | 9 | + | - | * | / | % | =
  3 | < | > | [ | ] | { | } | ( | )
  4 | ~ | \ | $ | | | & | _ | @ | #
  5 | ? | ! | . | : | ; | , | ' | "
  6 | reroll
  7 | reroll
  8 | reroll
~~~

See the entropy tables above to figure out how much entropy adding a
symbol adds to your passphrase. A reasonable estimate is about 10 bits
of extra entropy per added symbol.
